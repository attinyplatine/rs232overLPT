################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/fifo.c \
../src/helpers.c \
../src/lpt.c \
../src/rs232overLPT.c \
../src/usart.c 

OBJS += \
./src/fifo.o \
./src/helpers.o \
./src/lpt.o \
./src/rs232overLPT.o \
./src/usart.o 

C_DEPS += \
./src/fifo.d \
./src/helpers.d \
./src/lpt.d \
./src/rs232overLPT.d \
./src/usart.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


