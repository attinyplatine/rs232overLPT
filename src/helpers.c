/*
 * helpers.c
 *
 *  Created on: 26.05.2018
 *      Author: Fabian Spottog
 */


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
// Gibt die übergebene Zahl in binärer Schreibweise auf der Standardausgabe aus.
void binOutput(int zahlZumAusgeben){
    char tmpStr[9];
    for (int i = 0; i < 8; i++)
      	tmpStr[7-i] = '0' + ((zahlZumAusgeben >> i) & 0x1);
    tmpStr[8] = 0;
    puts(tmpStr);
}
// Wandelt einen String in eine Zeichenfolge, welche nur aus Kleinbuchstaben besteht, um.
void str2LowerCases(char *str){
	for(unsigned int i = 0; i < strlen(str); i++){
		str[i] = tolower(str[i]);
	}
}
// Dreht die Bits in dem übergebenen Byte um. Das erste wird das letzte Bit u.s.w.
char ReverseByte(char byte){
	byte = ((byte>>1)&0x55) | ((byte<<1)&0xAA);
	byte = ((byte>>2)&0x33) | ((byte<<2)&0xCC);
	byte = ((byte>>4)&0x0F) | ((byte<<4)&0xF0);
	return byte;
}
