/*
 * lpt.h
 *
 *  Created on: 26.05.2018
 *      Author: Fabian Spottog
 *      Funktionen für den direkten Zugriff auf die Hardware z.B: Setzen von Pegeln bzw. lesen des Zustandes.
 */

#ifndef LPT_H_
#define LPT_H_

#include <stdbool.h>
//die Adressen Festlegen.
#define LPT1	0x378 //Port 1
#define LPT2	0x278 //Port 2
#define LPT3	0x3BC //Port 3
// lptsetup(LPT1, LPT2, LPT3 oder direkt die Adresse eingeben)
//Mit Hilfe dieser Funktion wird die parallele Schnittstelle für den Zugriff initialisiert. Ohne deren Aufruf werden viele Funktionen nicht zur Verfügung stehen.
void lptsetup(int);
// Die einzelnen Threads, welche die Schreib- oder Leseoptionen ausführen müssen ebenfalls die Zugriffsberechtigung anfordern, sonst ist kein Zugriff möglich. Mithilfe des Parameters kann übergeben werden wie das Programm sich verhalten soll, sollte lptsetup noch nicht aufgerufen sein. Bei „false“ wird das Programm mit einem Fehler beendet, bei „true“ wird solange gewartet, bis der Aufruf erfolgt ist.
void lptThreadSetup(bool warteAufHardware);
// Gibt den Zustand des Status Ports wie der Port historisch noch heißt, maskiert mit der für uns relevanten Pin Maske zurück.
int lptstatusOhneMask(void);
// Gibt des Status Maskiert mit der übergebenen Maske zurück, so ist es möglich, auch die anderen Pins abzufragen, welche bisher noch unberücksichtigt sind.
int lptstatus(int mask);
// Diese Funktionen entsprechen den Funktionen lptstatusOhneMask bzw. lptstatus. Allerdings erfolgt die Ausgabe nicht als Rückgabewert, sondern die Ausgabe erfolgt ASCII codiert auf der Standardausgabe.
void lptPrintStatusOhneMask(void);
// Diese Funktionen entsprechen den Funktionen lptstatusOhneMask bzw. lptstatus. Allerdings erfolgt die Ausgabe nicht als Rückgabewert, sondern die Ausgabe erfolgt ASCII codiert auf der Standardausgabe.
void lptPrintStatus(int mask);
// Gibt den Pegel des Status Pins zurück. Die Routine zur Kommunikation greift auf diesen Programmaufruf zurück. Auf diese Funktion wirkt sich auch die Änderung des Pegels aus, welches von lptTurnRx initiiert wird.
bool lptgetBit(void);
// Setzt das Ausgangspin auf einen Signalzustand. Der elektrische Wert hängt von der mittels lptTurnTx getätigten Einstellung ab. Dieser Programmaufruf wird von der Kommunikationsroutine benutzt.
void lptsetBit(bool);
// Mit diesem Programmaufruf wird ein Wert in das Ausgangsbyte geschrieben. So können auch mehr als nur der eine Pin gesetzt werden.
void lptSendValue(int);
// Hier wird geprüft ob die Schnittstelle erfolgreich initialisiert ist. Falls nicht, wird das Programm mit einem Fehler beendet.
void lptCrashIfNotSetup(void);
// Abfrage ob die Schnittstelle erfolgreich initialisiert ist.
bool lptIsInit(void);
// Mithilfe dieser Programmaufrufe können die elektrischen Pegel an die Schaltung angepasst werden.
bool lptTurnRx(void);
// Mithilfe dieser Programmaufrufe können die elektrischen Pegel an die Schaltung angepasst werden.
bool lptTurnTx(void);
#endif /* LPT_H_ */
