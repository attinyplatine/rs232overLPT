/*
 * fifo.c
 *
 *  Created on: 26.05.2018
 *      Author: Fabian Spottog
 *		Ein Buffer, welcher nach dem „First in First out“ Prinzip arbeitet. Er bietet Platz je Richtung für 128 Byte.
 */
#include <stdbool.h>
#include <stdio.h>
#include <pthread.h>
#include "fifo.h"

#define FIFOSIZE 128
pthread_mutex_t rx_mutex, tx_mutex;
struct fifo {
	volatile char data[FIFOSIZE];
	volatile int readPointer;
	volatile int writePointer;
}
		tx_buffer= {{}, 0, 0},
		rx_buffer= {{}, 0, 0};
// Initialisierung, weil alle Programmteile über einen solchen verfügen, der Vollständigkeit halber implementiert, bei der fifo wäre dies nicht nötig.
void fifosetup(void){
}
//Prüfe ob Daten in den Sendebuffer geschrieben werden können.
bool checkTX(void){
	pthread_mutex_lock(&tx_mutex);
	bool ret =(tx_buffer.writePointer + 1 == tx_buffer.readPointer ) ||( tx_buffer.readPointer == 0 && tx_buffer.writePointer + 1 == FIFOSIZE );
	pthread_mutex_unlock(&tx_mutex);
	return ret;
}
// Fügt das übergebene Byte dem Sende Buffer hinzu. Wenn kein Platz in dem Buffer ist, wird solange gewartet, bis ein Byte gesendet wurde.
void fifoTxAdd(char toAdd){
	bool warnungGesendet = false;
	//warten bis Platz in Buffer ist.
	while(checkTX()){
		if(warnungGesendet){
			warnungGesendet = true;
			puts("Warnung: fifoTxAdd wartet auf Platz im Buffer\n");
		}
	}
	// Es kann noch zwischen dem Check und der eigentlichen Schreibaktion eine Thread-Umschaltung passieren.
	pthread_mutex_lock (&tx_mutex);
	//Füge Daten hinzu.
	tx_buffer.data[tx_buffer.writePointer] = toAdd;
	tx_buffer.writePointer++;
	if(tx_buffer.writePointer >= FIFOSIZE) tx_buffer.writePointer = 0;
	pthread_mutex_unlock(&tx_mutex);
}
// Schreibt das Älteste bzw. als Nächstes zusendende Byte in den übergebenen Speicherbereich. Über den booleschen Rückgabewert kann erkannt werden ob ein Wert vorhanden war.
bool fifoTxGet(char *receive){
	pthread_mutex_lock(&tx_mutex);
	// es sind keine aktuellen Daten vorhanden, dann false zurückgeben.
	  if (tx_buffer.readPointer == tx_buffer.writePointer){
		  pthread_mutex_unlock (&tx_mutex);
		  return false;
	  }
	  // daten zurückgeben
	  *receive = tx_buffer.data[tx_buffer.readPointer];
	  // die Datenzeiger manipulieren.
	  tx_buffer.readPointer++;
	  if (tx_buffer.readPointer >= FIFOSIZE)tx_buffer.readPointer = 0;
	  pthread_mutex_unlock (&tx_mutex);
	  return true;
}
//Prüfe ob Daten in den Lesebuffer geschrieben werden können.
bool checkRX(void){
	pthread_mutex_lock(&rx_mutex);
	bool ret =(rx_buffer.writePointer + 1 == rx_buffer.readPointer ) ||( rx_buffer.readPointer == 0 && rx_buffer.writePointer + 1 == FIFOSIZE );
	pthread_mutex_unlock(&rx_mutex);
	return ret;
}
// Fügt das übergebene Byte dem Empfangs Buffer hinzu. Wenn kein Platz in dem Buffer ist, wird solange gewartet, bis ein Byte entnommen wurde.
void fifoRxAdd(char toAdd){
	bool warnungGesendet = false;
	//warten bis Platz im Buffer ist.
	while(checkRX()){
		if(warnungGesendet){
			warnungGesendet = true;
			puts("Warnung: fifoRxAdd wartet auf Platz im Buffer\n");
		}
	}
	// Es kann noch zwischen dem Check und der eigentlichen Schreibaktion eine Thread-Umschaltung passieren.
	pthread_mutex_lock(&rx_mutex);
	//Füge Daten Hinzu.
	rx_buffer.data[rx_buffer.writePointer] = toAdd;
	rx_buffer.writePointer++;
	if(rx_buffer.writePointer >= FIFOSIZE) rx_buffer.writePointer = 0;
	pthread_mutex_unlock(&rx_mutex);
}
// Schreibt das Älteste bzw. als letztes empfangene Byte in den übergebenen Speicherbereich. Über den booleschen Rückgabewert kann erkannt werden, ob ein Wert vorhanden war.
bool fifoRxGet(char *recive){
	pthread_mutex_lock(&rx_mutex);
	// es sind keine aktuellen Daten vorhanden, dann false zurückgeben.
	  if (rx_buffer.readPointer == rx_buffer.writePointer){
		  pthread_mutex_unlock(&rx_mutex);
		  return false;
	  }
	  // Daten zurückgeben
	  *recive = rx_buffer.data[rx_buffer.readPointer];
	  // die Datenzeiger manipulieren.
	  rx_buffer.readPointer++;
	  if (rx_buffer.readPointer >= FIFOSIZE)rx_buffer.readPointer = 0;
	  pthread_mutex_unlock(&rx_mutex);
	  return true;
}
