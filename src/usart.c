/*
 * usart.c
 *
 *  Created on: 26.05.2018
 *      Author: Fabian Spottog
 *		Hier befinden sich die Programmaufrufe um über die virtuelle RS232 zu kommunizieren
 */
#include <stdbool.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "lpt.h"
#include "fifo.h"
#include "usart.h"
#include "helpers.h"


pthread_t readerThread, writerThread, consolenReadThread;
long Sleeptime;
int AnzStopBits;
static volatile bool BinOut = false, Textout = true, ErlaubeSenden;
// der Thread, welcher für das Lesen der Daten von der Seriellen Schnitstelle zuständig ist.
void *readThreadRun(void *void_ptr){
	struct timespec einViertelWarten = {0, ((Sleeptime*2) / 5) };
	struct timespec ganzKurzWarten = {0, (Sleeptime / 12) };
	struct timespec EinmalWarten = {0, Sleeptime };
	int WarteZaehlerBisErlaubeSenden = 0;
	lptThreadSetup(true); // Warte auf Hardware und starte den Init.
	while(true){
		nanosleep(&ganzKurzWarten, NULL);
		//suche Startbit
		if(lptgetBit()){
			ErlaubeSenden = false;
			WarteZaehlerBisErlaubeSenden = 0;
			nanosleep(&einViertelWarten, NULL); // noch einmal kurz Schlafen und somit ca in der mitte des High Pegel sein.
			char recive = 0;
			for(int i = 0; i < 8; i++){
				//jedes bit Einzeln Lesen
				nanosleep(&EinmalWarten, NULL);
				recive |= (lptgetBit() << i);
			}
			fifoRxAdd(recive);
			// einmal komplett warten, damit wir im Stopbit sind.
			for(int i = 0; i < AnzStopBits; i++)nanosleep(&EinmalWarten, NULL);
		}else{
			if(WarteZaehlerBisErlaubeSenden == 64)
				ErlaubeSenden = true;
			else
				WarteZaehlerBisErlaubeSenden++;
		}
	}
	return NULL;
}
//Liest den Fifo Puffer und gibt ihn auf der Console aus.
void *consolenReadThreadRun(void *void_ptr){
	char recive;
	while(true){
		usleep(Sleeptime / 100); // warte mindestens auf alle Bits mit Start und Stopzeit. Zu lange warten ist erlaubt, zu kurz verbraucht Resourcen.
		if(fifoRxGet(&recive)){
			//Daten empfangen.
			if(usartGetTextout()){
				putchar(recive);
			}else if(usartGetBinout()){
				binOutput(recive);
			}else{
				printf("%i\n", (unsigned char)recive);
			}
			fflush(stdout);
		}
	}
	return NULL;
}
// Thread welcher für die Ausgabe auf der Seriellen Schnittstelle zuständig ist.
void *writeThreadRun(void *void_ptr){
	struct timespec EinmalWarten = {0, Sleeptime };
	char toSend;
	lptThreadSetup(true); // Warte auf Hardware und starte den Init.
	// Definierter Zustand auf 0 setzen
	lptsetBit(false);
	while(true){
		nanosleep(&EinmalWarten, NULL);
		//Prüfe ob Daten vorhanden sind
		if(ErlaubeSenden && fifoTxGet(&toSend)){
			//Es sind Daten vorhanden zum Senden.
			//Sende Startbit.
			lptsetBit(true);nanosleep(&EinmalWarten, NULL);;
			//Jedes Bit
			for(int i = 0; i < 8; i++){
				lptsetBit(!((ReverseByte(toSend) >> i) & 0x1));
				nanosleep(&EinmalWarten, NULL);
			}
			lptsetBit(false);
			//Stoppbits senden.
			for(int i = 0; i < AnzStopBits; i++) nanosleep(&EinmalWarten, NULL);
		}
	}
	return NULL;
}
// Mithilfe der Setuproutine wird die Serielle Schnittstelle initialisiert. Dabei werden auch die für das Pollen notwendigen Threads gestartet. Das PC- Programm ist mittels Pollen realisiert, das Microcontroller- Programm mittels Interrupts um beide Möglichkeiten zu demonstrieren.
void usartsetup(int Baudrate, int StopBits){
	Sleeptime = 1000000000/Baudrate;
	AnzStopBits = StopBits;
	if(pthread_create( &readerThread, NULL, readThreadRun, NULL)) {
		perror("Thread kann leider nicht erzeugt werden.");
		exit(-2);
	}
	if(pthread_create( &writerThread, NULL, writeThreadRun, NULL)) {
		perror("Thread kann leider nicht erzeugt werden.");
		exit(-2);
	}
	if(pthread_create( &consolenReadThread, NULL, consolenReadThreadRun, NULL)) {
		perror("Thread kann leider nicht erzeugt werden.");
		exit(-2);
	}
}
// Gibt die Schlafenszeit in Nanosekunden der pollenden Schleifen zurück. Die Schlafenszeit errechnet sich: Sleeptime = 1.000.000.000 ÷ Baudrate
int usartGetSleeptime(){
	return Sleeptime;
}
// Ein einzelnes Zeichen wird in den Sendebuffer kopiert.
void usartSendChar(char toAdd){
	fifoTxAdd(toAdd);
}
// Eine Zeichenfolge wird in den Sendebuffer kopiert.
void usartSendString(char *toAdd){
    while (*toAdd){
    	usartSendChar(*toAdd);
    	toAdd++;
    }
}
// Die Ausgabe der empfangenen Zeichen kann auf verschiedene Arten ausgegeben werden. Mittels BinOut wird die binäre Zeichenfolge in 8 Bit-Blöcken auf der Standardausgabe ausgegeben.
bool usartGetBinout(){
	return BinOut;
}
// Setzt die binäre Ausgabe auf den übergebenen Wert.
bool usartSetBinout(bool BinOutput){
	BinOut = BinOutput;
	return BinOut;
}
// Toggelt die binäre Ausgabe. War die Ausgabe in binärer Form davor ausgeschaltet wird sie eingeschaltet. Wenn sie eingeschaltet war wird sie ausgeschaltet.
bool usartToggleBinout(){
	return usartSetBinout(!usartGetBinout());
}
// Eine weitere Ausgabeform ist die Ausgabe der ASCII Zeichen. Hier werden die Zeichen als Text interpretiert.
bool usartGetTextout(){
	return Textout;
}
// Setzt die Ausgabe in Textform auf den übergebenen Wert.
bool usartSetTextout(bool TextOut){
	Textout = TextOut;
	return Textout;
}
// Toggelt die Ausgabe in Textform. War die Ausgabe in Textform davor ausgeschaltet wird sie eingeschaltet. Wenn sie eingeschaltet war wird sie ausgeschaltet.
bool usartToggleTextout(){
	return usartSetTextout(!usartGetTextout());
}
