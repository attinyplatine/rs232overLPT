/*
 ============================================================================
 Name        : rs232overLPT.c
 Author      : Fabian Spottog
 Description : Dies ist der Einstiegspunkt des Programmes. Hier wird die Baudrate, Anzahl der Stoppbits definiert und der Anschluss gewählt. Folgende Funktionen sind in der C Datei programmiert.
 ============================================================================
 */
// für readline muss _GNU_SOURCE definiert sein dies ist default, readline hat den vorteil das die Zeichenkette belibieg groß sein kann.
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include "lpt.h"
#include "usart.h"
#include "helpers.h"

#define BAUDRATE 800
#define STOPBITS 2
#define ANSCHLUSS LPT1


bool ProgrammWeiter;
// Hier werden die Setup Routinen der verschiedenen Programmteile aufgerufen und das Programm somit initialisiert.
void setup(void);

//Als erstes wird die eigene Setuproutine aufgerufen und das Programm initialisiert. Anschließend wird in einer Endlosschleife mit dem Benutzer interagiert. Die Endlosschleife wird durch Eingabe des Kommandos „Exit“ beendet. Eine Übersicht über alle Befehle wird nach der Eingabe von „help“ ausgegeben. Groß- bzw. Kleinschreibung wird ignoriert. Auch die Kommandos an den Microcontroller werden vor dem Senden in Kleinbuchstaben umgewandelt.
int main(void) {
	setup();
	char *input = NULL;
	size_t len = 0;
	puts("rs232overLPT gestartet\nWarte auf Eingabe:\n");
	while(ProgrammWeiter){
		while(0 >= getline(&input, &len, stdin));
		str2LowerCases(input); // alles in Kleinbuchstaben umwandeln.
		if(strcmp(input, "exit\n") == 0){
			puts("bye bye\n");
			ProgrammWeiter = false;
		}else if((strcmp(input, "help\n") == 0) || (strcmp(input, "status\n") == 0)){
			printf("Baudrate %i, Stopbits: %i", BAUDRATE, STOPBITS);
			printf(" Sleeptime: %i\n", usartGetSleeptime());
			if(lptIsInit()) puts("Status: Verbunden\n"); else puts("Status: nicht Verbunden\n");
			puts("exit zum Beenden\n");
			puts("binout für eine Ausgabe im Binärformat\n");
			puts("help für diese Ausgabe\n");
		}else if(strcmp(input, "binout\n") == 0){
			usartSetTextout(false);
			if(usartToggleBinout()) puts("Binärausgabe Eingeschaltet\n"); else puts("Binärausgabe Ausgeschaltet\n");
		}else if(strcmp(input, "textout\n") == 0){
			usartSetBinout(false);
			if(usartToggleTextout()) puts("Textausgabe Eingeschaltet\n"); else puts("Textausgabe Ausgeschaltet\n");
		}else if(strcmp(input, "turnrx\n") == 0){
			if(lptTurnRx()) puts("RX gedreht\n"); else puts("RX Normal\n");
		}else if(strcmp(input, "turntx\n") == 0){
			if(lptTurnTx()) puts("TX gedreht\n"); else puts("TX Normal\n");
		}else if(strcmp(input, "input\n") == 0){
			puts("Input: ");binOutput(lptstatus(0xFF));if(lptgetBit()) puts(" Pin: True\n"); else puts("Pin: False\n");
		}else{
			usartSendString(input);
		}
	}
	return EXIT_SUCCESS;
}

//Hier werden die Setup Routinen der verschiedenen Programmteile aufgerufen und das Programm somit initialisiert.
void setup(void){
	ProgrammWeiter = true;
	usartsetup(BAUDRATE, STOPBITS);
	lptsetup(ANSCHLUSS);
}
