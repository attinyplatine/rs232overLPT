/*
 * usart.h
 *
 *  Created on: 26.05.2018
 *      Author: Fabian Spottog
 *      Hier befinden sich die Programmaufrufe um über die virtuelle RS232 zu kommunizieren
 */

#ifndef USART_H_
#define USART_H_
// Mithilfe der Setuproutine wird die Serielle Schnittstelle initialisiert. Dabei werden auch die für das Pollen notwendigen Threads gestartet. Das PC- Programm ist mittels Pollen realisiert, das Microcontroller- Programm mittels Interrupts um beide Möglichkeiten zu demonstrieren.
void usartsetup(int, int);
// Ein einzelnes Zeichen wird in den Sendebuffer kopiert.
void usartSendChar(char);
// Eine Zeichenfolge wird in den Sendebuffer kopiert.
void usartSendString(char *toAdd);
// Gibt die Schlafenszeit in Nanosekunden der pollenden Schleifen zurück. Die Schlafenszeit errechnet sich: Sleeptime = 1.000.000.000 ÷ Baudrate
int usartGetSleeptime(void);
// Die Ausgabe der empfangenen Zeichen kann auf verschiedene Arten ausgegeben werden. Mittels BinOut wird die binäre Zeichenfolge in 8 Bit-Blöcken auf der Standardausgabe ausgegeben.
bool usartGetBinout(void);
// Setzt die binäre Ausgabe auf den übergebenen Wert.
bool usartSetBinout(bool);
// Toggelt die binäre Ausgabe. War die Ausgabe in binärer Form davor ausgeschaltet wird sie eingeschaltet. Wenn sie eingeschaltet war wird sie ausgeschaltet.
bool usartToggleBinout(void);
// Eine weitere Ausgabeform ist die Ausgabe der ASCII Zeichen. Hier werden die Zeichen als Text interpretiert.
bool usartGetTextout(void);
// Setzt die Ausgabe in Textform auf den übergebenen Wert.
bool usartSetTextout(bool);
// Toggelt die Ausgabe in Textform. War die Ausgabe in Textform davor ausgeschaltet wird sie eingeschaltet. Wenn sie eingeschaltet war wird sie ausgeschaltet.
bool usartToggleTextout(void);
#endif /* USART_H_ */
