/*
 * lpt.c
 *
 *  Created on: 26.05.2018
 *      Author: Fabian Spottog
 *		Funktionen für den direkten Zugriff auf die Hardware z.B: Setzen von Pegeln bzw. lesen des Zustandes.
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/io.h>
#include <unistd.h>
#include <stdbool.h>
#include "helpers.h"
#include "lpt.h"
#define TXPIN 0b0000001
#define RXPIN 0x10
int Adresse;
#ifdef FAKELOOPBACK
	int Fakeloop = 0;
#endif
//Zum Anpassen des Pegels.
volatile bool txTurn, rxTurn;
//Registriere Destruktor.
void destructor (void) __attribute__((destructor));
// Mit Hilfe dieser Funktion wird die parallele Schnittstelle für den Zugriff initialisiert.
// Ohne deren Aufruf werden viele Funktionen nicht zur Verfügung stehen.
void lptsetup(int adresse){
	printf("Verbinde zu Adresse: 0x%x\n", adresse);
	puts("Hardware wird initialisiert\n");
	if (ioperm(adresse, 3, 1))
	    { perror("Error: Kann keine Verbindung zum Port herstellen."); exit(255); }
	Adresse = adresse;
}
//Die einzelnen Threads, welche die Schreib- oder Leseoptionen ausführen müssen ebenfalls die Zugriffsberechtigung anfordern, sonst ist kein Zugriff möglich. Mithilfe des Parameters kann übergeben werden wie das Programm sich verhalten soll, sollte lptsetup noch nicht aufgerufen sein. Bei „false“ wird das Programm mit einem Fehler beendet, bei „true“ wird solange gewartet, bis der Aufruf erfolgt ist.
void lptThreadSetup(bool warteAufHardware){
	if(warteAufHardware){
		while(!lptIsInit())usleep(100);//Warte auf Hardware
	}else{
		lptCrashIfNotSetup();
	}
	if (ioperm(Adresse, 3, 1))
	    { perror("Error: Thread kann keine Verbindung zum Port herstellen."); exit(255); }
}
// Abfrage ob die Schnittstelle erfolgreich initialisiert ist.
bool lptIsInit(void){
	return 0 != Adresse;
}
//Hier wird geprüft ob die Schnittstelle erfolgreich initialisiert ist. Falls nicht, wird das Programm mit einem Fehler beendet.
void lptCrashIfNotSetup(void){
	if(!lptIsInit()){
			perror("Error: es wurde noch kein Port festgelegt (lptsetup)."); exit(255);
		}
}
//Gibt den Zustand des Status Ports wie der Port historisch noch heißt, maskiert mit der für uns relevanten Pin Maske zurück.
int lptstatusOhneMask(void){
	return lptstatus(RXPIN);
}
//Gibt des Status Maskiert mit der übergebenen Maske zurück, so ist es möglich, auch die anderen Pins abzufragen, welche bisher noch unberücksichtigt sind.
int lptstatus(int mask){
	lptCrashIfNotSetup();
	#ifdef FAKELOOPBACK
		return Fakeloop & mask;
	#else
		return ((inb(Adresse + 1) ^ 0x80) >> 3) & mask;
	#endif
}
//Diese Funktionen entsprechen den Funktionen lptstatusOhneMask bzw. lptstatus. Allerdings erfolgt die Ausgabe nicht als Rückgabewert, sondern die Ausgabe erfolgt ASCII codiert auf der Standardausgabe.
void lptPrintStatusOhneMask(void){
	binOutput(lptstatusOhneMask());
}
//Diese Funktionen entsprechen den Funktionen lptstatusOhneMask bzw. lptstatus. Allerdings erfolgt die Ausgabe nicht als Rückgabewert, sondern die Ausgabe erfolgt ASCII codiert auf der Standardausgabe.
void lptPrintStatus(int mask){
	binOutput(lptstatus(mask));
}
//Mit diesem Programmaufruf wird ein Wert in das Ausgangsbyte geschrieben. So können auch mehr als nur der eine Pin gesetzt werden.
void lptSendValue(int toSend){
	lptCrashIfNotSetup();
	#ifdef FAKELOOPBACK
		Fakeloop = toSend;
	#else
		outb(toSend, Adresse);
	#endif

}
//Wird automatisch beim Verlassen des Programmes aufgerufen. Ein Aufruf von außerhalb ist nicht vorgesehen, hier wird der Parallel Port wieder freigegeben.
void destructor(void){
	if(0 != Adresse) ioperm(Adresse, 3, 0);
	Adresse = 0;
}
//Gibt den Pegel des Status Pins zurück. Die Routine zur Kommunikation greift auf diesen Programmaufruf zurück. Auf diese Funktion wirkt sich auch die Änderung des Pegels aus, welches von lptTurnRx initiiert wird.
bool lptgetBit(void){
	if(rxTurn){
		if(0 == lptstatusOhneMask()) return false; else return true;
	}else{
		if(0 == lptstatusOhneMask()) return true; else return false;
	}
}
//Setzt das Ausgangspin auf einen Signalzustand. Der elektrische Wert hängt von der mittels lptTurnTx getätigten Einstellung ab. Dieser Programmaufruf wird von der Kommunikationsroutine benutzt.
void lptsetBit(bool toSend){
	if(txTurn){
		if(toSend) // Umgedreht da High Pegel der Ruhepegel ist.
			lptSendValue(TXPIN);
		else
			lptSendValue(0);
	}else{
		if(!toSend)
			lptSendValue(TXPIN);
		else
			lptSendValue(0);
	}
}
// Mithilfe dieser Programmaufrufe können die elektrischen Pegel an die Schaltung angepasst werden.
bool lptTurnRx(){
	rxTurn = !rxTurn;
	return rxTurn;
}
// Mithilfe dieser Programmaufrufe können die elektrischen Pegel an die Schaltung angepasst werden.
bool lptTurnTx(){
	txTurn = !txTurn;
	return txTurn;
}
