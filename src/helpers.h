/*
 * helpers.h
 *
 *  Created on: 26.05.2018
 *      Author: Fabian Spottog
 */

#ifndef HELPERS_H_
#define HELPERS_H_
// Gibt die übergebene Zahl in binärer Schreibweise auf der Standardausgabe aus.
void binOutput(int);
// Wandelt einen String in eine Zeichenfolge, welche nur aus Kleinbuchstaben besteht, um.
void str2LowerCases(char *str);
// Dreht die Bits in dem übergebenen Byte um. Das erste wird das letzte Bit u.s.w.
char ReverseByte(char);
#endif /* HELPERS_H_ */
